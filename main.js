var mymap = L.map("mapid").setView([-34.6, -58.38], 5);
var markers = [];
let locationsInfo = [];

L.tileLayer(
  "https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw",
  {
    maxZoom: 18,
    attribution:
      'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
      '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
      'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    id: "mapbox/streets-v11",
    tileSize: 512,
    zoomOffset: -1
  }
).addTo(mymap);

const getLocations = () => {
  fetch("data.json")
    .then(response => response.json())
    .then(locations => {
      locations.forEach(location => {
        myMarker = L.marker(location, {
          title: "Resource location",
          alt: "Resource Location",
        })
          .bindPopup(
            "<br><b>Punto de interes: " +
              location.nombre +
              "</b><br>Direccion: " +
              location.direccion +
              "<br>Telefono: " +
              location.telefono +
              "<br>Categoria: " +
              location.categoria +
              "<br>( x, y ): " +
              location.lat +
              ", " +
              location.lng +
              "<br><br><button class='btn btn-danger' onclick='clearMarker(" +
              location.lat +
              location.lng +
              ")'> Borrar </button>"
          )
          .openPopup();
        myMarker._id = location.lat + location.lng;
        markers.push(myMarker);
        mymap.addLayer(myMarker);
      });
    });
};

function addMarker(
  lat,
  lng,
  direccion,
  telefono,
  descripcion,
  categoria,
  nombre
) {
  var location = {
    lat: lat,
    lng: lng,
    descripcion: descripcion,
    nombre: nombre,
    direccion: direccion,
    telefono: telefono,
    categoria: categoria
  };
  var myMarker = L.marker(location, {
    title: "Resource location",
    alt: "Resource Location",
  })
    .bindPopup(
      "<br><b>Punto de interes: " +
        location.nombre +
        "</b><br>Direccion: " +
        location.direccion +
        "<br>Telefono: " +
        location.telefono +
        "<br>Categoria: " +
        location.categoria +
        "<br>( x, y ): " +
        location.lat +
        ", " +
        location.lng +
        "<br><br><button class='btn btn-danger' onclick='clearMarker(" +
        location.lat +
        location.lng +
        ")'> Borrar </button>"
    )
    .openPopup();
  myMarker._id = location.lat + location.lng;
  markers.push(myMarker);
  mymap.addLayer(myMarker);
}

function clearMarker(id) {
  var new_markers = [];
  markers.forEach(function(marker) {
    if (marker._id == id) mymap.removeLayer(marker);
    else new_markers.push(marker);
  });
  markers = new_markers;
}

function onMapClick(e) {
  document.getElementById("lat").value = e.latlng.lat;
  document.getElementById("lng").value = e.latlng.lng;
}

mymap.on("click", onMapClick);

function submitted(event) {
  event.preventDefault();
  var nombre = document.getElementById("nombre").value;
  var direccion = document.getElementById("direccion").value;
  var telefono = document.getElementById("telefono").value;
  var descripcion = document.getElementById("descripcion").value;
  var categoria = document.getElementById("categoria").value;
  var lat = document.getElementById("lat").value;
  var lng = document.getElementById("lng").value;
  lat = parseFloat(lat);
  lng = parseFloat(lng);
  addMarker(lat, lng, direccion, telefono, descripcion, categoria, nombre);
  document.getElementById("form").reset();
}

window.addEventListener("load", getLocations);
